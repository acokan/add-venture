from models import UserProfile


class CustomUserAuth(object):

    def authenticate(self, username=None, password=None):
        try:
            user = UserProfile.objects.get(email=username, password=password)
            return user
        except UserProfile.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            user = UserProfile.objects.get(pk=user_id)
            if user.is_active:
                return user
            return None
        except UserProfile.DoesNotExist:
            return None
