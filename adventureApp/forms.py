from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from models import UserProfile


class CustomUserCreationForm(UserCreationForm):
    """
    Forma koja kreira user-a, bez privilegija, od unesenog mail-a i password-a
    """

    def __init__(self, *args, **kargs):
        super(CustomUserCreationForm, self).__init__(*args, **kargs)
        del self.fields['username']

    class Meta:
        model = UserProfile
        fields = ("email",)

class CustomUserChangeForm(UserChangeForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    def __init__(self, *args, **kargs):
        super(CustomUserChangeForm, self).__init__(*args, **kargs)
        del self.fields['username']

    class Meta:
        model = UserProfile