from django.contrib import admin

from .models import (
    Skills,
    Industry,
    LessonCategory,
    Lesson,
    Content,
    Questions)

admin.site.register(Skills)
admin.site.register(Content)
admin.site.register(Industry)
admin.site.register(Lesson)
admin.site.register(LessonCategory)
admin.site.register(Questions)
