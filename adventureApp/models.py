from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User, PermissionsMixin
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.mail import send_mail


# Create your models here.


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Kreira i cuva user-a sa email-om i password-om
        """
        if not email:
            raise ValueError('The given email must be set')

        email = self.normalize_email(email)
        user = self.model(email=email)
        user.set_password(password)
        user.save(using=self._db)
        return user


class Prerequisite(models.Model):
    BOOLEAN_CHOICES = (
        (True, "Yes"),
        (False, "No")
    )
    answer1 = models.BooleanField(choices=BOOLEAN_CHOICES, default=False)
    answer2 = models.BooleanField(choices=BOOLEAN_CHOICES, default=False)
    answer3 = models.BooleanField(choices=BOOLEAN_CHOICES, default=False)
    answer4 = models.BooleanField(choices=BOOLEAN_CHOICES, default=False)
    answer5 = models.BooleanField(choices=BOOLEAN_CHOICES, default=False)
    answer6 = models.BooleanField(choices=BOOLEAN_CHOICES, default=False)
    answer7 = models.BooleanField(choices=BOOLEAN_CHOICES, default=False)


class Industry(models.Model):
    name = models.CharField("Industry name", max_length=50)

    def __unicode__(self):
        return self.name


class Track(models.Model):
    TRACK = (
        ('bmd', 'Organizational/Business model development'),
        ('pd', 'Project Development'),
    )
    name = models.CharField(max_length=150, choices=TRACK, default='/', null=True, blank=True)
    answer1 = models.CharField("Answer on 1. question", max_length=150, null=True, blank=True)
    answer2 = models.CharField("Answer on 2. question", max_length=150, null=True, blank=True)
    answer3 = models.CharField("Answer on 3. question", max_length=150, null=True, blank=True)
    answer4 = models.CharField("Answer on 4. question", max_length=150, null=True, blank=True)
    tag = models.CharField("Hash tag chosen by user", max_length=50, null=True, blank=True)
    industry = models.ForeignKey(Industry, null=True, blank=True)


class UserProfile(AbstractBaseUser):
    name = models.CharField(max_length=50, null=True, blank=True)
    surname = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(max_length=50, null=True, blank=True, unique=True)
    age = models.IntegerField(null=True, blank=True)
    img_path = models.ImageField(upload_to='img-profile', max_length=10000, null=True)
    city = models.CharField(max_length=50, null=True, blank=True)
    state = models.CharField(max_length=50, null=True, blank=True)
    fb_link = models.CharField("Facebook url", max_length=50, null=True, blank=True)
    ln_link = models.CharField("LinkedIn url", max_length=50, null=True, blank=True)
    team = models.BooleanField("Does it have team or not", default=False)
    id_prerequisite = models.ForeignKey(Prerequisite, null=True)
    id_track = models.ForeignKey(Track, null=True)
    is_admin = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=True)
    share = models.CharField(max_length=50, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    # Vise ne koristimo default user manager nego custom koji smo gore napravili
    # Kad pozovemo UserProfile.objects.all() koristice metode iz CustomUserManager()
    objects = CustomUserManager()

    # Embedovana klasa u okviru UserProfil-a koja sadrzi meta podatke
    # class Meta:
    #     verbose_name = _('user')
    #     verbose_name_plural = _('users')

    # def get_absolute_url(self):
    #     return "/users/%s/" % urlquote(self.email)

    # def get_pk(self):
    #     return self.pk

    def get_full_name(self):
        # Vraca puno ime.
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        # Vraca ime
        return self.name

    # def email_user(self, subject, message, from_email=None):
    #     # Salje mail korisniku. Trebace nam kasnije za one PDF-ove
    #     send_mail(subject, message, from_email, [self.email])

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True


class Skills(models.Model):
    name = models.CharField("Skill name", max_length=50)

    def __unicode__(self):
        return self.name


class UserSkills(models.Model):
    id_user = models.ForeignKey(settings.AUTH_USER_MODEL)
    id_skills = models.ForeignKey(Skills)


class References(models.Model):
    name = models.CharField("Reference name", max_length=50)
    description = models.CharField("Previous project description", max_length=160)
    link = models.CharField("Link to the website", max_length=50)
    id_user = models.ForeignKey(settings.AUTH_USER_MODEL)


class CanvasCategory(models.Model):
    name = models.CharField("Canvas category name", max_length=50)

    def __unicode__(self):
        return self.name


class Notes(models.Model):
    title = models.CharField("Note title", max_length=50)
    text = models.TextField(max_length=500)
    id_user = models.ForeignKey(settings.AUTH_USER_MODEL)
    id_canvas_category = models.ForeignKey(CanvasCategory)

    #this limits the text to 50 characters
    def return_text_gist(self):
        return self.text[:50]

    def return_category(self):
        return self.id_canvas_category


class LessonCategory(models.Model):
    name = models.CharField("Lesson Category name", max_length=50)
    points = models.IntegerField(null=True, blank=True)
    description_pm = models.TextField("Chapter Description PM",null=True)
    description_org = models.TextField("Chapter Description ORG",null=True)
    logo = models.ImageField(max_length=1000, null=True)

    def __unicode__(self):
        return self.name


class Lesson(models.Model):
    title = models.CharField("Lesson title", max_length=100)
    summary = models.TextField(null=True)
    points = models.IntegerField(null=True, blank=True)
    track = models.ForeignKey(Track)
    lesson_category = models.ForeignKey(LessonCategory)

    def return_with_space(self):
        long = len(self.title)
        string = "                                                                         "
        x = string[long:]
        return self.title+x

    def __unicode__(self):
        return self.title


class LessonRecords(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    lesson = models.ForeignKey(Lesson)
    points = models.IntegerField(null=True, blank=True)


class ContentType(models.Model):
    name = models.CharField(max_length=50)


class Content(models.Model):
    text = models.TextField("Text", max_length=2000)
    lesson = models.ForeignKey(Lesson)
    content_type = models.ForeignKey(ContentType)

    def __unicode__(self):
        return self.text


class Questions(models.Model):
    question_text = models.CharField("Question text", max_length=140)
    lesson = models.ForeignKey(Lesson)
    canvas_category = models.ForeignKey(CanvasCategory,default=1)

    def __unicode__(self):
        return self.question_text


class CategoryCompletion(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    lesson_category = models.ForeignKey(LessonCategory)
    status = models.BooleanField()


class CanvasAnswers(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    lesson_category = models.ForeignKey(LessonCategory)
    question = models.ForeignKey(Questions)
    answer = models.TextField(max_length=500)
    points = models.IntegerField()


class Notification(models.Model):
    question = models.ForeignKey(Questions, null=True, default=None)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    type = models.CharField(max_length=50)
    lesson_category = models.ForeignKey(LessonCategory, null=True)

    def clean_category_name(self, word):
        word = word.lower()
        if "/" in word:
            words = word.split("/")
            word = words[0] + "-" + words[1]
            return word
        if " " in word:
            word.lower()
            words = word.split(" ")
            word = words[0] + "-" + words[1]
            return word
        else:
            return word
        return word