from bokeh.properties import String
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, render_to_response
from django.contrib import auth
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
import random, re
from django.db import transaction
from addventure import settings
from .models import (
    CanvasCategory,
    Notes,
    Skills,
    UserProfile,
    References,
    UserSkills,
    Track,
    Industry,
    LessonCategory,
    Lesson,
    Content,
    LessonRecords,
    Prerequisite,
    CategoryCompletion,
    Questions,
    CanvasAnswers,
    Notification)

#removes all unalphabet components of string
def leave_alpha(x):
    regex = re.compile('[^a-zA-Z]')
    return regex.sub('', x)
# returns random digit - 0,1...9
def return_random_digit(x,y):
    return random.randint(x,y)
# returns a string of 4 random digits
def return_four_digit_sting(x,y):
    return str(return_random_digit(x,y))+str(return_random_digit(x,y))\
           +str(return_random_digit(x,y))+str(return_random_digit(x,y))

# returns username before @, for example if username is "aco@gmail.com" it will return "aco"
def crop_username(username):
    return username.split("@")[0]

def return_track_by_name(track_name):
    # returns 10 or 13 as track id based on track name   track_name -> request.user.id_track.name
    if track_name == "Project Development":
        return 10
    elif track_name == "Organizational/Business model development":
        return 13

# this method changes "lesson" to "Lesson" or "mission-vision" to "Mission/Vision"
def alter(word):
    if word == "target-group":
        words = word.split("-")
        word = words[0].capitalize() + " " + words[1].capitalize()
        return word
    elif "-" in word:
        words = word.split("-")
        word = words[0].capitalize() + "/" + words[1].capitalize()
        # print word
        return word
    else:
        # print word.capitalize()
        return word.capitalize()

def cat_slug(word):
    if word == "Project Goal/s":
        return "project-goal-s"
    elif len(word.split(" ")) == 1:
        return word.lower()
    else:
        word = word.replace(" ", "-")
        words = word.split("-")
        stringW = ""
        for w in words:
             stringW += w.lower()+"-"
        return stringW.strip("-")

def alter_category(word):
    if word == "means-of-funding":
        return "Means of Funding"
    elif word == "project-goal-s":
        return "Project Goal/s"
    elif "-" in word:
        words = word.split("-")
        word = words[0].capitalize() + " " + words[1].capitalize()
        return word
    else:
        return word.capitalize()

def r_alter(word):
    if "/" in word:
        word = word.split("/")
        word = word[0].lower()+"-"+word[1].lower()
        return word
    else:
        return word.lower()

def clean_category_name(word):
    if "/" in word:
        words = word.split("/")
        word = words[0] + "-" + words[1]
        return word
    if " " in word:
        words = word.split(" ")
        word = words[0] + "-" + words[1]
        return word
    else:
        return word


# this method returns css class name, word="red", css_class_name="red-div"
def return_css_class(word):
    return word + "-divs"


# this method returns the percentage of completion overall
def overall_progress(user):
    track = Track.objects.get(id=return_track_by_name(user.id_track.name))
    lessons = Lesson.objects.filter(track_id=return_track_by_name(track.name))
    completed_lessons = LessonRecords.objects.filter(user_id=user)

    result = 0
    if len(lessons) != 0:

        for lesson in completed_lessons:
            result += lesson.points
        return result
    else:
        return 0


# this method returns the percentage of completion for each chapter
def chapter_progress(user, category_name):
    category_name = alter(category_name)
    category = LessonCategory.objects.get(name=category_name)
    track = Track.objects.get(id=return_track_by_name(user.id_track.name))
    lessons = Lesson.objects.filter(lesson_category_id=category, track_id=return_track_by_name(track.name))
    completed_lessons = LessonRecords.objects.filter(user_id=user)

    def is_completed(lesson):
        for com_les in completed_lessons:
            if lesson.id == com_les.lesson_id:
                return True
        return False

    result = 0
    for les in lessons:
        if is_completed(les):
            result += float(les.points*100)/category.points

    if len(lessons) != 0:
        return round(result, 2)
    else:
        return 0

# Returns canvas progress float
def canvas_progress(user):
    num_answers = CanvasAnswers.objects.filter(user=user).count()

    if num_answers == 0:
        return 0

    if user.id_track.name == "Project Development":
        return round(float(num_answers)/27*100,2)
    else:
        return round(float(num_answers)/26*100,2)

# Create your views here.
def home(request):
    return render(request, "home.html")


def about(request):
    return render(request, "about.html")


def login(request):
    return render(request, "login.html")


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/login/')


def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)
    message = "Username or password are incorrect"
    ctx = {"user": user, "message": "Username or password are incorrect"}

    if user is not None:
        auth.login(request, user)
        request.session["active_user"] = user.email
        request.session.set_expiry(0)
        return HttpResponseRedirect('/dashboard/')
    else:
        return HttpResponseRedirect('/login/')


def registration(request):
    skills = Skills.objects.all()
    ctx = {"skills": skills}
    return render(request, "sign-up.html", ctx)


def registration2(request):
    return render(request, "sign-up-2.html")


def registration3(request):
    industry = Industry.objects.all()
    ctx = {"industry": industry}
    return render(request, "sign-up-3.html", ctx)


def signup(request):

    if request.method == 'POST':

        name = request.POST.get("name", "")
        surname = request.POST.get("surname", "")
        age = request.POST.get("age", "")
        try:
            int(age)
        except:
            return render(request, 'sign-up.html', {"error": "Age field must be a number"})

        email = request.POST.get("email", "")
        password1 = request.POST.get("password1", "")
        password2 = request.POST.get("password2", "")
        img = request.FILES.get("file", "")

        if password1 != password2:
            return render(request, "sign-up.html", {"error": "Your passwords did not match"})

        fb = request.POST.get("fb-link", "")
        ln = request.POST.get("linkedin-link", "")
        city = request.POST.get("city", "")
        state = request.POST.get("state", "")

        share = leave_alpha(crop_username(email)) + "-" + return_four_digit_sting(0, 9)

        try:
            check_user = UserProfile.objects.get(email=email)
        except:
            check_user = None

        if check_user is not None:
            return render(request, "sign-up.html", {"error": email+" e-mail address is already used. If your "
                                                                   "registration was not completed, login "
                                                                   "with " + email + " e-mail and chosen password and "
                                                                                     "then registrate again!"})
        else:
            user = UserProfile(password=password1, name=name, surname=surname, email=email,
                               age=age, img_path=img, city=city, state=state, fb_link=fb,
                               ln_link=ln, share=share, is_admin=False, is_superuser=False)
            user.save()

        request.session["reg_user"] = email

        skill1 = request.POST.get("skill1", "")
        skill2 = request.POST.get("skill2", "")
        skill3 = request.POST.get("skill3", "")
        skill4 = request.POST.get("skill4", "")
        skill5 = request.POST.get("skill5", "")

        if skill1 != "skill...":
            skills1 = UserSkills(id_user=user, id_skills=Skills.objects.get(name=skill1))
            skills1.save()

        if skill2 != "skill...":
            skills2 = UserSkills(id_user=user, id_skills=Skills.objects.get(name=skill2))
            skills2.save()

        if skill3 != "skill...":
            skills3 = UserSkills(id_user=user, id_skills=Skills.objects.get(name=skill3))
            skills3.save()

        if skill4 != "skill...":
            skills4 = UserSkills(id_user=user, id_skills=Skills.objects.get(name=skill4))
            skills4.save()

        if skill5 != "skill...":
            skills5 = UserSkills(id_user=user, id_skills=Skills.objects.get(name=skill5))
            skills5.save()

        ref_name_1 = request.POST.get("project-name1", "")
        ref_description_1 = request.POST.get("project-description1", "")
        ref_link_1 = request.POST.get("project-link1", "")

        ref_name_2 = request.POST.get("project-name2", "")
        ref_description_2 = request.POST.get("project-description2", "")
        ref_link_2 = request.POST.get("project-link2", "")

        ref_name_3 = request.POST.get("project-name3", "")
        ref_description_3 = request.POST.get("project-description3", "")
        ref_link_3 = request.POST.get("project-link3", "")

        if ref_name_1 != "" and ref_description_1 != "" and ref_link_1 != "":
            references1 = References(name=ref_name_1, description=ref_description_1, link=ref_link_1, id_user=user)
            references1.save()

        if ref_name_2 != "" and ref_description_2 != "" and ref_link_2 != "":
            references2 = References(name=ref_name_2, description=ref_description_2, link=ref_link_2, id_user=user)
            references2.save()

        if ref_name_3 != "" and ref_description_3 != "" and ref_link_3 != "":
            references3 = References(name=ref_name_3, description=ref_description_3, link=ref_link_3, id_user=user)
            references3.save()

    return HttpResponseRedirect("/sign-up-2/")


def signup2(request):

    if "reg_user" in request.session:
        user_email = request.session.get("reg_user")

        if request.method == "POST":

            track_radio = request.POST["track"]
            track = Track(name=track_radio)
            track.save()
            request.session["track"] = track.pk

            team = request.POST["team"]
            user = UserProfile.objects.get(email=user_email)

            user.team = team
            user.id_track_id = track.pk
            user.save()

    return HttpResponseRedirect('/sign-up-3/')


def signup3(request):
    if request.method == 'POST':
        if "track" in request.session:
            track_id = request.session["track"]
            track = Track.objects.get(id=track_id)

            name_b_or_i = request.POST.get("nameBorI", "")
            what_problem = request.POST.get("what_problem", "")
            products = request.POST.get("products", "")
            whose_problem = request.POST.get("whose_problem", "")
            tag = request.POST.get("tag", "")
            industry_name = request.POST.get("industry", "")

            if industry_name != "industry...":
                track.answer1 = name_b_or_i
                track.answer2 = what_problem
                track.answer3 = products
                track.answer4 = whose_problem
                track.tag = tag
                track.industry_id = Industry.objects.get(name=industry_name).pk
            else:
                track.answer1 = name_b_or_i
                track.answer2 = what_problem
                track.answer3 = products
                track.answer4 = whose_problem
                track.tag = tag

            track.save()

    return HttpResponseRedirect('/prerequisite/')


def prerequisite(request):
    if "reg_user" in request.session:
        user_email = request.session["reg_user"]
        user = UserProfile.objects.get(email=user_email)
        track = Track.objects.get(id=return_track_by_name(user.id_track.name))
        ctx = {"track": track}
        return render(request, "prerequisite.html", ctx)

    return render(request, "prerequisite.html")


def is_true(value):
    if value == "Yes":
        return True
    else:
        return False


def save_prerequisite(request):
    if "reg_user" in request.session:
        user_email = request.session["reg_user"]
        user = UserProfile.objects.get(email=user_email)

        if request.method == "POST":
            answer1 = request.POST.get("firstQuestion", "")
            answer2 = request.POST.get("secondQuestion", "")
            answer3 = request.POST.get("thirdQuestion", "")
            answer4 = request.POST.get("fourthQuestion", "")
            answer5 = request.POST.get("fifthQuestion", "")
            answer6 = request.POST.get("sixthQuestion", "")
            answer7 = request.POST.get("seventhQuestion", "")

            pr = Prerequisite(answer1=is_true(answer1), answer2=is_true(answer2), answer3=is_true(answer3),
                              answer4=is_true(answer4), answer5=is_true(answer5), answer6=is_true(answer6),
                              answer7=is_true(answer7))
            pr.save()

            user.id_prerequisite_id = pr.id
            user.save()

    return HttpResponseRedirect('/login/')


def save_notification(user, q, notification_type):
    current_notification = Notification.objects.filter(user=user, question=q, type=notification_type)
    if not current_notification:
        notification = Notification(user=user, question=q, type=notification_type)
        notification.save()


def save_notification_prerequisite(user, lc, notification_type):
    current_notification = Notification.objects.filter(user=user, lesson_category_id=lc, type=notification_type)
    if not current_notification:
        notification = Notification(user=user, lesson_category_id=lc, type=notification_type)
        notification.save()


# checks if there are completed chapters and save them in CategoryCompletion table
def save_completion(user, cat_id):
    if chapter_progress(user, LessonCategory.objects.get(id=cat_id).name) == float(100):
        if len(CategoryCompletion.objects.filter(user=user, lesson_category=LessonCategory.objects.get(id=cat_id))) == 0:
            completion = CategoryCompletion(lesson_category=LessonCategory.objects.get(id=cat_id), user=user, status=1)
            completion.save()


# this method completes a chapter
def complete_chapter(user, category_id):
    category = LessonCategory.objects.get(id=category_id)
    print category
    track_obj = Track.objects.get(id=return_track_by_name(user.id_track.name))
    print track_obj.name
    lessons = Lesson.objects.filter(lesson_category=category, track=track_obj)
    print len(lessons)
    records = LessonRecords.objects.filter(user=user)

    l = LessonRecords.objects.filter(lesson=lessons[0], user=user).exists()
    print "lesson: " + lessons[0].title
    print
    print(l)
    if l is False:
        for lesson in lessons:
            print(lesson.lesson_category.name)
            lesson_records = LessonRecords()
            lesson_records.points = lesson.points
            lesson_records.lesson = lesson
            lesson_records.user = user
            lesson_records.save()
    else:
        return


@login_required
def dashboard(request):
    user_email = request.session["active_user"]
    user = UserProfile.objects.get(email=user_email)
    user_skills = UserSkills.objects.filter(id_user=user)
    skills = Skills.objects.all()
    skills_list = []
    for user_skill in user_skills:
        for skill in skills:
            if user_skill.id_skills_id == skill.id:
                skills_list.append(skill)
    try:
        track = Track.objects.get(id=user.id_track.id)
    except:
        user.delete()
        return render(request, "dashboard.html", {"error": "Your registration was not complete. Please, registrate "
                                                           "yourself again using same email and password"})
    references = References.objects.filter(id_user=user)
    if track.industry is not None:
        industry = Industry.objects.get(id=track.industry_id)
    else:
        industry = ""

    # Save prerequisite notifications
    try:
        prerequisites = Prerequisite.objects.get(id=user.id_prerequisite_id)
    except:
        user.delete()
        track.delete()
        return render(request, "dashboard.html", {"error": "Your registration was not complete. Please, registrate "
                                                           "yourself again using same email and password"})
    notification_type_pre = "prerequisite"

    if prerequisites.answer1:
        complete_chapter(user, 1)
    else:
        save_notification_prerequisite(user, 1, notification_type_pre)

    if track.name == "Project Development":
        if prerequisites.answer2:
            complete_chapter(user, 9)
        else:
            save_notification_prerequisite(user, 9, notification_type_pre)
    else:
        if prerequisites.answer2:
            complete_chapter(user, 2)
        else:
            save_notification_prerequisite(user, 2, notification_type_pre)

    if prerequisites.answer3:
        complete_chapter(user, 3)
    else:
        save_notification_prerequisite(user, 3, notification_type_pre)

    if prerequisites.answer4:
        complete_chapter(user, 4)
    else:
        save_notification_prerequisite(user, 4, notification_type_pre)

    if prerequisites.answer5:
        complete_chapter(user, 5)
    else:
        save_notification_prerequisite(user, 5, notification_type_pre)

    if track.name == "Project Development":
        if prerequisites.answer6:
            complete_chapter(user, 10)
        else:
            save_notification_prerequisite(user, 10, notification_type_pre)
    else:
        if prerequisites.answer6:
            complete_chapter(user, 6)
        else:
            save_notification_prerequisite(user, 6, notification_type_pre)

    if prerequisites.answer7:
        complete_chapter(user, 7)
    else:
        save_notification_prerequisite(user, 7, notification_type_pre)

    # save completion if completed
    for i in [1, 2, 3, 4, 5, 6, 7, 9, 10]:
        save_completion(user, i)

    # Save question notifications
    notification_type = "canvas-question"
    cat_completion = CategoryCompletion.objects.filter(user=user)
    for cat in cat_completion:
        if cat.status:
            if cat.lesson_category_id == 1:
                if track.name == "Project Development":
                    for q in Questions.objects.filter(lesson_id=23):
                        save_notification(user, q, notification_type)
                else:
                    for q in Questions.objects.filter(lesson_id=17):
                        save_notification(user, q, notification_type)
            if cat.lesson_category_id == 2:
                for q in Questions.objects.filter(lesson_id=68):
                    save_notification(user, q, notification_type)
            if cat.lesson_category_id == 3:
                if track.name == "Project Development":
                    for q in Questions.objects.filter(lesson_id=35):
                        save_notification(user, q, notification_type)
                else:
                    for q in Questions.objects.filter(lesson_id=73):
                        save_notification(user, q, notification_type)
            if cat.lesson_category_id == 4:
                if track.name == "Project Development":
                    for q in Questions.objects.filter(lesson_id=47):
                        save_notification(user, q, notification_type)
                else:
                    for q in Questions.objects.filter(lesson_id=83):
                        save_notification(user, q, notification_type)
            if cat.lesson_category_id == 6:
                for q in Questions.objects.filter(lesson_id=93):
                    save_notification(user, q, notification_type)
            if cat.lesson_category_id == 7:
                if track.name == "Project Development":
                    for q in Questions.objects.filter(lesson_id=60):
                        save_notification(user, q, notification_type)
                else:
                    for q in Questions.objects.filter(lesson_id=101):
                        save_notification(user, q, notification_type)
            if cat.lesson_category_id == 9:
                for q in Questions.objects.filter(lesson_id=29):
                    save_notification(user, q, notification_type)
            if cat.lesson_category_id == 10:
                for q in Questions.objects.filter(lesson_id=54):
                    save_notification(user, q, notification_type)

    # Overall progress
    op = overall_progress(user)

    # Progress by chapters
    track_obj = Track.objects.get(id=return_track_by_name(track.name))
    lesson_cate = Lesson.objects.filter(track=track_obj)
    category_list = set()
    progress_by_category = {}
    list_cat = []
    for category in lesson_cate:
        category_list.add(category.lesson_category.name)
    for category in category_list:
        progress = chapter_progress(user, category)
        progress_by_category['category'] = category
        progress_by_category['progress'] = progress
        progress_by_category['color'] = return_css_class(clean_category_name(category))
        list_cat.append(progress_by_category.copy())

    share_field = user.share.split("-")
    username = share_field[0]
    digits = share_field[1]

    canvasCategories = CanvasCategory.objects.all()

    notifications = Notification.objects.filter(user=user)
    all_answers = CanvasAnswers.objects.filter(user_id=user)

    prerequisite_notifications = []
    completed_notifications = []
    not_completed_notifications = []

    if all_answers.count() == 0:
        for notification in notifications:
            if notification.type == "canvas-question":
                not_completed_notifications.append(notification)

    for answer in all_answers:
        for notification in notifications:
            if notification.type == "canvas-question" and notification.question == answer.question:
                completed_notifications.append(notification)

    for notification in notifications:
        if notification.type == "canvas-question":
            if (notification not in not_completed_notifications) and (notification not in completed_notifications):
                not_completed_notifications.append(notification)
        else:
            notification.lesson_category.name = notification.clean_category_name(notification.lesson_category.name)
            prerequisite_notifications.append(notification)

    canvas_pr = canvas_progress(user)

    ctx = {"user": user, "skills": skills_list, "track": track, "references": references, "industry": industry,
           "notifications": notifications, "overall_progress": op, "category_progress": list_cat, "username": username,
           "digits": digits, "categories": canvasCategories, "completed_not": reversed(completed_notifications),
           "not_completed_not": not_completed_notifications, "prerequisite_not": prerequisite_notifications,
           "can_progress": canvas_pr}
    return render(request, "dashboard.html", ctx)


@login_required()
def save_canvas_answer(request):
    if request.method == 'POST':
        user_email = request.session["active_user"]
        user = UserProfile.objects.get(email=user_email)

        answer = request.POST.get("answer", "")
        q = request.POST.get("question_id", "")
        question = Questions.objects.get(id=q)

        ca = CanvasAnswers(answer=answer, user=user, question=question, lesson_category=question.lesson.lesson_category,
                           points=0)
        ca.save()

    return HttpResponseRedirect("/canvas/")


@login_required()
def edit_canvas_answer(request, id):
    if request.method == 'POST':
        answer = CanvasAnswers.objects.get(id=id,user=request.user)
        answer.answer = request.POST.get("answer", "")
        answer.save()

    return HttpResponseRedirect("/canvas/")


@login_required
def edit_profile(request):
    user_email = request.session["active_user"]
    user = UserProfile.objects.get(email=user_email)
    references = References.objects.filter(id_user=user)
    user_skills = UserSkills.objects.filter(id_user=user)
    skills = Skills.objects.all()
    skills_list = []
    for user_skill in user_skills:
        for skill in skills:
            if user_skill.id_skills_id == skill.id:
                skills_list.append(skill)
    list_length = len(skills_list)
    if list_length > 0:
        request.session["user_skill_1"] = skills_list[0].id
    if list_length > 1:
        request.session["user_skill_2"] = skills_list[1].id
    if list_length > 2:
        request.session["user_skill_3"] = skills_list[2].id
    if list_length > 3:
        request.session["user_skill_4"] = skills_list[3].id
    if list_length > 4:
        request.session["user_skill_5"] = skills_list[4].id

    references_length = len(references)
    if references_length > 0:
        request.session["ref_1"] = references[0].name
    if references_length > 1:
        request.session["ref_2"] = references[1].name
    if references_length > 2:
        request.session["ref_3"] = references[2].name

    ctx = {"user": user, "references": references, "skills_list": skills_list, "skills": skills,
           "list_length": list_length}
    return render(request, "edit-profile.html", ctx)


@login_required
def save_profile(request):

    if request.method == 'POST':
        user_email = request.session["active_user"]
        user = UserProfile.objects.get(email=user_email)

        skill1 = request.POST.get("skill1", "")
        skill2 = request.POST.get("skill2", "")
        skill3 = request.POST.get("skill3", "")
        skill4 = request.POST.get("skill4", "")
        skill5 = request.POST.get("skill5", "")

        if (skill1 == skill2 and skill1 != "skill...") or (skill1 == skill3 and skill1 != "skill...") or (skill1 == skill4 and skill1 != "skill...") or (skill1 == skill5 and skill1 != "skill...") or (skill2 == skill3 and skill2 != "skill...") or (skill2 == skill4 and skill2 != "skill...") or (skill2 == skill5 and skill2 != "skill...") or (skill3 == skill4 and skill3 != "skill...") or (skill3 == skill5 and skill3 != "skill...") or (skill4 == skill5 and skill4 != "skill..."):
            return render(request, "edit-profile.html", {"error": "You can not have two or more same skills!"})

        if "user_skill_1" in request.session:
            skills1 = UserSkills.objects.get(id_skills_id=request.session["user_skill_1"], id_user_id=user.id)
            if skill1 == "skill...":
                skills1.delete()
            else:
                id_skill1 = Skills.objects.get(name=skill1)
                skills1.id_skills = id_skill1
                skills1.save()
            del request.session["user_skill_1"]
        else:
            if skill1 != "skill...":
                skills1 = UserSkills(id_skills=Skills.objects.get(name=skill1), id_user=user)
                skills1.save()

        if "user_skill_2" in request.session:
            skills2 = UserSkills.objects.get(id_skills_id=request.session["user_skill_2"], id_user=user)
            if skill2 == "skill...":
                skills2.delete()
            else:
                id_skill2 = Skills.objects.get(name=skill2)
                skills1.id_skills = id_skill2
                skills2.save()
            del request.session["user_skill_2"]
        else:
            if skill2 != "skill...":
                skills2 = UserSkills(id_skills=Skills.objects.get(name=skill2), id_user=user)
                skills2.save()
        if "user_skill_3" in request.session:
            skills3 = UserSkills.objects.get(id_skills_id=request.session["user_skill_3"], id_user=user)
            if skill3 == "skill...":
                skills3.delete()
            else:
                id_skill3 = Skills.objects.get(name=skill3)
                skills3.id_skills = id_skill3
                skills3.save()
            del request.session["user_skill_3"]
        else:
            if skill3 != "skill...":
                skills3 = UserSkills(id_skills=Skills.objects.get(name=skill3), id_user=user)
                skills3.save()

        if "user_skill_4" in request.session:
            skills4 = UserSkills.objects.get(id_skills_id=request.session["user_skill_4"], id_user=user)
            if skill4 == "skill...":
                skills4.delete()
            else:
                id_skill4 = Skills.objects.get(name=skill4)
                skills4.id_skills = id_skill4
                skills4.save()
            del request.session["user_skill_4"]
        else:
            if skill4 != "skill...":
                skills4 = UserSkills(id_skills=Skills.objects.get(name=skill4), id_user=user)
                skills4.save()

        if "user_skill_5" in request.session:
            skills5 = UserSkills.objects.get(id_skills_id=request.session["user_skill_5"], id_user=user)
            if skill5 == "skill...":
                skills5.delete()
            else:
                id_skill5 = Skills.objects.get(name=skill5)
                skills5.id_skills = id_skill5
                skills5.save()
            del request.session["user_skill_5"]
        else:
            if skill5 != "skill...":
                skills5 = UserSkills(id_skills=Skills.objects.get(name=skill5), id_user=user)
                skills5.save()

        ref_name_1 = request.POST.get("project-name1", "")
        ref_description_1 = request.POST.get("project-description1", "")
        ref_link_1 = request.POST.get("project-link1", "")

        ref_name_2 = request.POST.get("project-name2", "")
        ref_description_2 = request.POST.get("project-description2", "")
        ref_link_2 = request.POST.get("project-link2", "")

        ref_name_3 = request.POST.get("project-name3", "")
        ref_description_3 = request.POST.get("project-description3", "")
        ref_link_3 = request.POST.get("project-link3", "")

        if "ref_1" in request.session:
            user_ref_1 = References.objects.get(name=request.session["ref_1"])
            user_ref_1.name = ref_name_1
            user_ref_1.description = ref_description_1
            user_ref_1.link = ref_link_1
            user_ref_1.save()
        else:
            user_ref_1 = References(name=ref_name_1, description=ref_description_1, link=ref_link_1, id_user=user)
            user_ref_1.save()

        if "ref_2" in request.session:
            user_ref_2 = References.objects.get(name=request.session["ref_2"])
            user_ref_2.name = ref_name_2
            user_ref_2.description = ref_description_2
            user_ref_2.link = ref_link_2
            user_ref_2.save()
        else:
            user_ref_2 = References(name=ref_name_2, description=ref_description_2, link=ref_link_2, id_user=user)
            user_ref_2.save()

        if "ref_3" in request.session:
            user_ref_3 = References.objects.get(name=request.session["ref_3"])
            user_ref_3.name = ref_name_3
            user_ref_3.description = ref_description_3
            user_ref_3.link = ref_link_3
            user_ref_3.save()
        else:
            user_ref_3 = References(name=ref_name_3, description=ref_description_3, link=ref_link_3, id_user=user)
            user_ref_3.save()

        if user.name != request.POST.get("name", ""):
            user.name = request.POST.get("name", "")

        if user.surname != request.POST.get("surname", ""):
            user.surname = request.POST.get("surname", "")

        if user.age != request.POST.get("age", ""):
            user.age = request.POST.get("age", "")

        if str(request.FILES.get("file", "")) != "":
            user.img_path = request.FILES.get("file", "")

        if user.fb_link != request.POST.get("fb-link", ""):
            user.fb_link = request.POST.get("fb-link", "")

        if user.ln_link != request.POST.get("linkedin-link", ""):
            user.ln_link = request.POST.get("linkedin-link", "")

        if user.city != request.POST.get("city", ""):
            user.city = request.POST.get("city", "")

        if user.state != request.POST.get("state", ""):
            user.state = request.POST.get("state", "")

        password1 = request.POST.get("password1", "")
        password2 = request.POST.get("password2", "")

        if password1 != password2:
            return render(request, "edit-profile.html", {"error": "Your passwords did not match"})

        if user.password != password1:
            user.password = password1
            user.save()
            return HttpResponseRedirect("/login/")

        user.save()

    return HttpResponseRedirect("/dashboard/")


@login_required
def search(request):

    users = []
    if request.method == 'POST':
        parameter = request.POST.get("parameter", "")
        value = request.POST.get("search", "")
        if value != "":
            if parameter == "Name":
                all_users = UserProfile.objects.all()
                for user in all_users:
                    value_1 = value.lower()
                    value_2 = value.capitalize()
                    if user.name.startswith(value_1) or user.name.startswith(value_2):
                        users.append(user)
            elif parameter == "Surname":
                all_users = UserProfile.objects.all()
                for user in all_users:
                    value_1 = value.lower()
                    value_2 = value.capitalize()
                    if user.surname.startswith(value_1) or user.surname.startswith(value_2):
                        users.append(user)
            elif parameter == "Place":
                all_users = UserProfile.objects.all()
                for user in all_users:
                    value_1 = value.lower()
                    value_2 = value.capitalize()
                    if user.city.startswith(value_1) or user.city.startswith(value_2):
                        users.append(user)
            elif parameter == "Skill":
                all_skills = Skills.objects.filter(name=value)
                for skill in all_skills:
                    user_skills = UserSkills.objects.filter(id_skills=skill.id)
                    for s in user_skills:
                        users = UserProfile.objects.filter(id=s.id_user_id)
            elif parameter == "Industry":
                all_industries = Industry.objects.filter(name=value)
                for ind in all_industries:
                    tracks = Track.objects.filter(industry=ind)
                    for track in tracks:
                        users = UserProfile.objects.filter(id_track=track)
            elif parameter == "Tag":
                tracks = Track.objects.filter(tag=value)
                for track in tracks:
                    users = UserProfile.objects.filter(id_track=track)

    ctx = {"users": users}

    return render(request, "search.html", ctx)


def share(request, share_profile, digits):

    share_field = share_profile + "-" + digits

    user = UserProfile.objects.get(share=share_field)
    track = Track.objects.get(id=return_track_by_name(user.id_track.name))

    try:
        industry = Industry.objects.get(id=track.industry_id)
    except:
        industry = None

    cat_dict = {
        "problem": 0,
        "solution": 1,
        "target_group": 2,
        "project_goal": 3,
        "channels": 4,
        "unfair_advantage": 5,
        "means_of_funding": 6,
        "cost_structure": 7,
        "key_indicators": 8,
    }

    answers = CanvasAnswers.objects.filter(user_id=user).only("id", "question", "user").order_by("question_id")
    categories = CanvasCategory.objects.all().order_by("id")

    def get_category(category_name):
        return categories[cat_dict[category_name]]

    def get_answers(cat):
        ans = []
        for a in answers:
            if a.question.canvas_category == cat:
                ans.append(a)
        return ans

    url = request.build_absolute_uri()

    ctx = {
        "problem": get_answers(get_category("problem")),
        "solution": get_answers(get_category("solution")),
        "target_group": get_answers(get_category("target_group")),
        "project_goal": get_answers(get_category("project_goal")),
        "channels": get_answers(get_category("channels")),
        "unfair_advantage": get_answers(get_category("unfair_advantage")),
        "means_of_funding": get_answers(get_category("means_of_funding")),
        "cost_structure": get_answers(get_category("cost_structure")),
        "key_indicators": get_answers(get_category("key_indicators")),
        "user": user,
        "track": track,
        "industry": industry,
        "url": url
    }

    return render(request, "share.html", ctx)


@login_required
def delete(request):
    user_email = request.session["active_user"]
    user = UserProfile.objects.get(email=user_email)
    track = Track.objects.get(id=return_track_by_name(user.id_track.name))

    try:
        industry = Industry.objects.get(id=track.industry_id)
    except:
        industry = None

    cat_dict = {
            "problem": 0,
            "solution": 1,
            "target_group": 2,
            "project_goal": 3,
            "channels": 4,
            "unfair_advantage": 5,
            "means_of_funding": 6,
            "cost_structure": 7,
            "key_indicators": 8,
        }

    answers = CanvasAnswers.objects.filter(user_id=request.user).only("id", "question", "user").order_by("question_id")
    categories = CanvasCategory.objects.all().order_by("id")

    def get_category(category_name):
        return categories[cat_dict[category_name]]

    def get_answers(cat):
        ans = []
        for a in answers:
            if a.question.canvas_category == cat:
                ans.append(a)
        return ans

    notes = Notes.objects.all()

    ctx = {
        "problem": get_answers(get_category("problem")),
        "solution": get_answers(get_category("solution")),
        "target_group": get_answers(get_category("target_group")),
        "project_goal": get_answers(get_category("project_goal")),
        "channels": get_answers(get_category("channels")),
        "unfair_advantage": get_answers(get_category("unfair_advantage")),
        "means_of_funding": get_answers(get_category("means_of_funding")),
        "cost_structure": get_answers(get_category("cost_structure")),
        "key_indicators": get_answers(get_category("key_indicators")),
        "user": user,
        "track": track,
        "industry": industry,
        "notes": notes
    }

    return render(request, "delete.html", ctx)


def delete_profile(request):
    user = UserProfile.objects.get(email=request.user.email)

    references = References.objects.filter(id_user=user)
    user_skills = UserSkills.objects.filter(id_user=user)

    skills = Skills.objects.all()
    skills_list = []
    for user_skill in user_skills:
        for skill in skills:
            if user_skill.id_skills_id == skill.id:
                skills_list.append(skill)
    list_length = len(skills_list)
    if list_length > 0:
        request.session["user_skill_1"] = skills_list[0].id
    if list_length > 1:
        request.session["user_skill_2"] = skills_list[1].id
    if list_length > 2:
        request.session["user_skill_3"] = skills_list[2].id
    if list_length > 3:
        request.session["user_skill_4"] = skills_list[3].id
    if list_length > 4:
        request.session["user_skill_5"] = skills_list[4].id

    references_length = len(references)
    if references_length > 0:
        request.session["ref_1"] = references[0].name
    if references_length > 1:
        request.session["ref_2"] = references[1].name
    if references_length > 2:
        request.session["ref_3"] = references[2].name

    ctx = {
        "context": "delete",
        "user": user,
        "references": references,
        "skills_list": skills_list,
        "skills": skills,
        "list_length": list_length
    }

    user_track = Track.objects.get(id=user.id_track_id)
    user_prerequisite = Prerequisite.objects.get(id=user.id_prerequisite_id)

    user.delete()
    user_track.delete()
    user_prerequisite.delete()
    for ref in references:
        ref.delete()

    auth.logout(request)

    return render(request, 'sign-up.html', ctx)


@login_required
def add_note(request):
    if request.method == "POST":
        title = request.POST.get("note-title")
        text = request.POST.get("note-text")
        category = request.POST.get("category")

        note = Notes()
        note.title = title
        note.text = text
        cat = CanvasCategory.objects.get(id=category)
        note.id_canvas_category = cat
        note.id_user = request.user
        note.save()

        #return HttpResponseRedirect('/notes/all/')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    canvasCategories = CanvasCategory.objects.all()

    context = {
        "categories": canvasCategories,
    }
    return render(request, "add-note.html", context)

@login_required
def edit_note(request, id):
    note = Notes.objects.get(id=id)
    note.title = request.POST.get('note-title', '')
    note.text = request.POST.get('note-text', '')
    category = request.POST.get("category")
    cat = CanvasCategory.objects.get(id=category)
    note.id_canvas_category = cat
    note.save()
    #return HttpResponseRedirect('/notes/all/')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required
def notes(request, slug):

    def set_cats(cat, name):
        if name == "":
            cats = {}
            cats["name"] = cat.name
            cats["slug"] = cat_slug(cat.name)
            cats["css"] = return_css_class(cat_slug(cat.name))
            cats["col"] = set_col(cat.name)
        else:
            cats = {}
            cats["name"] = name
            cats["slug"] = cat_slug(cat.name)
            cats["css"] = return_css_class(cat_slug(cat.name))
            cats["col"] = set_col(name)
        return cats

    def set_col(word):
        if len(word) > 14:
            return "col-md-2"
        else:
            return "col-md-1"

    user = request.user
    categories = CanvasCategory.objects.all()
    cats_list = []
    if user.id_track.name == "Project Development":
        for cat in categories:
            print cat
            cats = set_cats(cat,"")
            #print cats
            cats_list.append(cats)

    else:
        for cat in categories:
            if cat.name == "Key Indicators":
                cats = set_cats(cat, "Key Metrics")
                cats_list.append(cats)
            elif cat.name == "Means of Funding":
                cats = set_cats(cat, "Revenue Streams")
                cats_list.append(cats)
            elif cat.name == "Target Group":
                cats = set_cats(cat, "Target Market")
                cats_list.append(cats)
            elif cat.name == "Project Goal/s":
                cats = set_cats(cat,"Unique Value")
                cats_list.append(cats)
            else:
                cats = set_cats(cat,"")
                cats_list.append(cats)

    category = alter_category(slug)
    not_all = False

    if category == "All":
        notes = Notes.objects.filter(id_user = user)
        not_all = True

    if not_all == False:
        cat = categories.filter(name=category)
        notes = Notes.objects.filter(id_user=user,id_canvas_category=cat)
    #categories = CanvasCategory.objects.all()
    notes_list = []
    css_dict = {
        "problem": "problem-div",
        "solution": "solution-div",
        "target_group": "target-group-div",
        "project_goals": "project-goals-div",
        "channels": "channels-div",
        "unfair_advantage": "unfair-advantage-div",
        "means_of_funding": "means-of-funding-div",
        "cost_structure": "cost-structure-div",
        "key_indicators": "key-indicators-div",
    }

    active = alter_category(slug)

    context = {
        "notes": notes,
        "cats": cats_list,
        "categories": categories,
        "css_dict": css_dict,
        "active": active,
        "slug": slug
    }
    return render(request, "notes.html", context)


@login_required
def note_detail(request, id):
    instance = get_object_or_404(Notes, id=id)

    context = {
        "note": instance,
    }
    return render(request, "note_detail.html", context)


@login_required
def learn(request):
    user = request.user
    chapters = LessonCategory.objects.all()
    img_names = [
        "mission_vision.png",
        "community_market.png",
        "communication.png",
        "finances.png",
        "leadership.png",
        "product_service.png",
        "execution.png"
    ]
    op = overall_progress(user)
    canvasCategories = CanvasCategory.objects.all()

    # Progress by chapters
    track = Track.objects.get(id=user.id_track.id)

    mission_vision_progress = chapter_progress(user, "Mission/Vision")
    comm_progress = chapter_progress(user, "Communication")
    finances_progress = chapter_progress(user, "Finances")
    leadership_progress = chapter_progress(user, "Leadership")
    execution_progress = chapter_progress(user, "Execution")
    target_group_progress = {}
    problem_solution_progress = {}
    comm_market_group_progress = {}
    product_service_solution_progress = {}

    if track.name == "Project Development":
        target_group_progress = chapter_progress(user, "Target Group")
        problem_solution_progress = chapter_progress(user, "Problem/Solution")
    else:
        comm_market_group_progress = chapter_progress(user, "Community/Market")
        product_service_solution_progress = chapter_progress(user, "Product/Service")

    context = {
        "chapters": chapters,
        "img_names": img_names,
        "overall_progress": op,
        "categories": canvasCategories,
        "mission_vision_progress": mission_vision_progress,
        "comm_progress": comm_progress,
        "finances_progress": finances_progress,
        "leadership_progress": leadership_progress,
        "execution_progress": execution_progress,
        "target_group_progress": target_group_progress,
        "problem_solution_progress": problem_solution_progress,
        "comm_market_group_progress": comm_market_group_progress,
        "product_service_solution_progress": product_service_solution_progress
    }
    return render(request, "learn.html", context)


@login_required
def chapter(request, slug):
    css_class = return_css_class(slug)

    images = {
        "mission-vision": LessonCategory.objects.get(id=1).logo.url,
        "community-market": LessonCategory.objects.get(id=2).logo.url,
        "communication": LessonCategory.objects.get(id=3).logo.url,
        "finances": LessonCategory.objects.get(id=4).logo.url,
        "leadership": LessonCategory.objects.get(id=5).logo.url,
        "product-service": LessonCategory.objects.get(id=6).logo.url,
        "execution": LessonCategory.objects.get(id=7).logo.url,
        "problem-solution":LessonCategory.objects.get(id=10).logo.url,
        "target-group":LessonCategory.objects.get(id=9).logo.url,
    }

    colors = {
        "communication": "#FFF427",
        "community-market": "#B8F629",
        "target-group":"#B8F629",
        "execution": "#004C4A",
        "finances": "#3CA2FF",
        "leadership": "#F43900",
        "mission-vision": "#CF0A2C",
        "product-service": "#9E005D",
        "problem-solution":"#9E005D",
    }

    color = colors[slug]
    img = images[slug]
    r_slug = slug
    slug = alter(slug)
    category = LessonCategory.objects.get(name=slug)
    lessons = Lesson.objects.filter(lesson_category_id=category, track_id=return_track_by_name(request.user.id_track.name)).order_by('id')
    completed_lessons = LessonRecords.objects.filter(user_id=request.user).order_by('lesson_id')

    def is_completed(lesson):
        for com_les in completed_lessons:
            if lesson.id == com_les.lesson_id:
                return True
        return False
    joined_lessons = []

    for les in lessons:
        if is_completed(les):
            completed = True
            l_list = []
            l_list.append(les)
            l_list.append(completed)
            joined_lessons.append(l_list)
        else:
            completed = False
            l_list = []
            l_list.append(les)
            l_list.append(completed)
            joined_lessons.append(l_list)

    completed_percent = chapter_progress(request.user, r_slug)

    if request.user.id_track.name == "Project Development":
        description = category.description_pm
    else:
        description = category.description_org

    canvasCategories = CanvasCategory.objects.all()

    context = {
        "lessons": lessons,
        "title": slug,
        "css_class": css_class,
        "img": img,
        "completed_lessons": completed_lessons,
        "color": color,
        "joined_lessons": joined_lessons,
        "completed_percent": completed_percent,
        "description": description,
        "categories": canvasCategories,
    }

    return render(request, "chapter.html", context)


@login_required
def lesson_detail(request, slug, id):
    lesson = get_object_or_404(Lesson, id=id)
    r_slug = slug
    slug = alter(slug)

    completed = False
    completed_lessons = LessonRecords.objects.filter(user_id=request.user)
    for com_les in completed_lessons:
        if com_les.lesson_id == lesson.id:
            completed = True

    lesson_content = Content.objects.filter(lesson_id=id).order_by('lesson_id')
    # slug = alter(slug)
    canvasCategories = CanvasCategory.objects.all()

    wrong_track = False
    if return_track_by_name(request.user.id_track) != lesson.track.id:
        wrong_track = True

    context = {
        "lesson": lesson,
        "slug": slug,
        "contents": lesson_content,
        "completed": completed,
        "r_slug": r_slug,
        "categories": canvasCategories,
        "wrong_track": wrong_track,
    }
    return render(request, "lesson_detail.html", context)


@login_required
def complete_lesson(request, slug, id):

    #slug = alter(slug)

    if request.method == 'POST':
        lesson_id = id
        lesson = Lesson.objects.get(id=id)
        user = request.user
        checkbox = request.POST.get("lesson-completed")

        if checkbox == "Yes":
            record = LessonRecords()
            record.user = user
            record.lesson = lesson
            record.points = lesson.points
            record.save()
        if chapter_progress(user, lesson.lesson_category.name) == float(100):
            return HttpResponseRedirect('/dashboard/')
        else:
            return HttpResponseRedirect('/learn/' + slug + '/')


@login_required
def canvas(request):
    import time
    # time = time.time()
    def print_time(time):
        if time > 60:
            print 'It took', time/60, 'minutes.'
        else:
            print 'It took', time, 'seconds.'
    start = time.time()

    def get_question(id):
        return Questions.objects.get(id=id)

    cat_dict = {
            "problem":0,
            "solution": 1,
            "target_group": 2,
            "project_goal": 3,
            "channels": 4,
            "unfair_advantage": 5,
            "means_of_funding": 6,
            "cost_structure": 7,
            "key_indicators": 8,
        }

    answers = CanvasAnswers.objects.filter(user_id=request.user).only("id","question","user").order_by("question_id")
    categories = CanvasCategory.objects.all().order_by("id")

    def get_category(category_name):
        return categories[cat_dict[category_name]]
    print "ANS"
    patka = time.time()-start
    print_time(patka)

    def get_answers(cat):
        ans = []

        for a in answers:
            if a.question.canvas_category == cat:
                ans.append(a)
                #answers.remove(a)

            patka = time.time()-start
            print_time(patka)
        return ans

    all_answers = CanvasAnswers.objects.filter(user_id=request.user).only("id", "question", "user", "answer").order_by("id")
    canvas_categories = CanvasCategory.objects.all()

    # Notifications
    notifications = Notification.objects.filter(user=request.user)
    all_answers = CanvasAnswers.objects.filter(user_id=request.user)

    completed_notifications = []
    not_completed_notifications = []

    if all_answers.count() == 0:
        for notification in notifications:
            if notification.type == "canvas-question":
                not_completed_notifications.append(notification)

    for answer in all_answers:
        for notification in notifications:
            if notification.type == "canvas-question" and notification.question == answer.question:
                completed_notifications.append(notification)

    for notification in notifications:
        if notification.type == "canvas-question":
            if (notification not in not_completed_notifications) and (notification not in completed_notifications):
                not_completed_notifications.append(notification)

    context = {
        "problem": get_answers(get_category("problem")),
        "solution": get_answers(get_category("solution")),
        "target_group":get_answers(get_category("target_group")),
        "project_goal":get_answers(get_category("project_goal")),
        "channels":get_answers(get_category("channels")),
        "unfair_advantage":get_answers(get_category("unfair_advantage")),
        "means_of_funding":get_answers(get_category("means_of_funding")),
        "cost_structure":get_answers(get_category("cost_structure")),
        "key_indicators":get_answers(get_category("key_indicators")),
        "all_answers": all_answers,
        "categories": canvas_categories,
        "completed_not": reversed(completed_notifications),
        "not_completed_not": not_completed_notifications
    }

    return render(request, "canvas.html", context)

def discover(request):
    def is_completed(lesson):
            comp = LessonRecords.objects.filter(user_id=request.user,lesson=lesson).exists()
            if comp == True:
                    return True
            return False

    if overall_progress(request.user) < float(100):

        #completed_lessons = LessonRecords.objects.filter(user_id=request.user)
        all_lessons = list(Lesson.objects.filter(track_id=return_track_by_name(request.user.id_track.name)))

        for l in all_lessons:
            if is_completed(l):
                all_lessons.remove(l)
                print len(all_lessons)

        access_index = return_random_digit(0,len(all_lessons)-1)
        lesson = all_lessons[access_index]
        while is_completed(lesson) == True :
            access_index = return_random_digit(0,len(all_lessons)-1)
            lesson = all_lessons[access_index]

        lesson_content = Content.objects.filter(lesson_id=lesson.id).order_by('lesson_id')


        if is_completed(lesson):
            completed = True
        else:
            completed = False

        canvasCategories = CanvasCategory.objects.all()

        context = {
            "lesson": lesson,
            "contents": lesson_content,
            "completed": completed,
            "r_slug": r_alter(lesson.lesson_category.name),
            "slug": alter(lesson.title),
            "categories": canvasCategories,
        }

        return render(request, "lesson_detail.html", context)
    else:
        return render(request, "no_discovery.html")

def forgotten_pass(request):

    return render(request, "forgotten_password.html")

def send_pass(request):
    no_user = False
    user = None
    email = request.POST.get("mail")
    try:
        user = UserProfile.objects.get(email=email)
    except UserProfile.DoesNotExist:
        no_user = True

    context = {
        "email":email,
        "no_user": no_user,
        "user":user
    }

    if no_user == False:
        subject = "Add+venture - Password recovery"
        our_mail = "stefanvujovic93@gmail.com"
        user_mail = user.email
        txt = """
        Hello there!

        This is your password at www.add-venture.net: {}

        Cheers!

        The Add+venture Team
        www.add-venture.net
        """.format(user.password)
        send_mail(subject,txt,our_mail,[user_mail],fail_silently=False,)

    return render(request, "confirmation_pass.html", context)


def help(request):
    return render(request, "help.html")

