from __future__ import unicode_literals

from django.apps import AppConfig


class AdventureappConfig(AppConfig):
    name = 'adventureApp'
