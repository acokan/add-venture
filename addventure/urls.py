"""add_venture URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, patterns, include
from django.conf.urls.static import static
from django.contrib import admin

from adventureApp import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import settings

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^about/$', views.about, name='about'),
    url(r'^login/$', views.login, name='login'),
    url(r'^login/auth/$', views.auth_view, name='auth'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^sign-up/$', views.registration, name='registration'),
    url(r'^sign-up/next/$', views.signup, name='signup'),
    url(r'^sign-up-2/$', views.registration2, name='registration2'),
    url(r'^sign-up-2/next/$', views.signup2, name='signup2'),
    url(r'^sign-up-3/$', views.registration3, name='registration3'),
    url(r'^sign-up-3/next/$', views.signup3, name='signup3'),
    url(r'^prerequisite/$', views.prerequisite, name='prerequisite'),
    url(r'^prerequisite/next/$', views.save_prerequisite, name='save-prerequisite'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^dashboard/save-answer/$', views.save_canvas_answer, name='save-canvas-answer'),
    url(r'^dashboard/edit-answer/(?P<id>\d+)/$', views.edit_canvas_answer, name='edit-canvas-answer'),
    url(r'^edit-profile/$', views.edit_profile, name='edit-profile'),
    url(r'^edit-profile/next/$', views.save_profile, name='save-profile'),
    url(r'^add-note/$', views.add_note, name="add_note"),
    url(r'^edit-note/(?P<id>\d+)/$', views.edit_note, name="edit_note"),
    url(r'^notes/(?P<slug>[\w-]+)/$', views.notes, name="notes"),
    url(r'^notes/(?P<id>\d+)/$', views.note_detail, name="note_detail"),
    url(r'^learn/$', views.learn, name="learn"),
    url(r'^learn/(?P<slug>[\w-]+)/$', views.chapter, name="chapter"),
    url(r'^learn/(?P<slug>[\w-]+)/(?P<id>\d+)/$', views.lesson_detail, name="lesson_detail"),
    url(r'^completed/(?P<slug>[\w-]+)/(?P<id>\d+)/$', views.complete_lesson, name="complete"),
    url(r'^canvas/$', views.canvas, name="canvas"),
    url(r'^search/$', views.search, name="search"),
    url(r'^share/(?P<share_profile>[\w-]+)/(?P<digits>\d{4})/$', views.share, name="share"),
    url(r'^delete/$', views.delete, name="delete"),
    url(r'^delete-profile/$', views.delete_profile, name="delete_profile"),
    url(r'^discover/$', views.discover, name="discover"),
    url(r'^forgotten-password/$', views.forgotten_pass, name="forgotten_pass"),
    url(r'^send-pass/$', views.send_pass, name="send_pass"),
    url(r'^help/$', views.help, name="help"),
    #url(r'^learn/(?P<slug>[\w-]+)/(?P<id>\d+)/$'),views.lesson_detail, name="lesson_detail"),
    #url(r'^learn/(?P<slug>[\w-]+)/(?P<id>\d+)/$'), views.lesson_detail, name="lesson_detail"),
    url(r'^admin/', admin.site.urls, name='admin'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
